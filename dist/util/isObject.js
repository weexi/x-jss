/**
 * 
 * @param v
 * @returns {boolean}
 */
module.exports = function (v) {
  return !!v && typeof v === 'object'
}
