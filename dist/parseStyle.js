var toLowerCase = require('./util/toLowerCase')

var pseudoKey = [
  'hover',
  'link',
  'visited',
  'after',
  'before',
  'lang',
  'checked',
  'disabled',
  'enabled',
  'invalid',
  'root',
  'target',
  'focus',
  'blur',
  'active'
];

/**
 * @description
 * @param selector
 * @param style
 * @returns {{children: Array, property: Array, selector: *, pseudo: {}}}
 */
function parseStyle(selector, style) {
  var cssList = {
    property: [],
    pseudo: {},
    children: [],
    selector: selector
  };
  for(var name in style) {
    var item = style[name];
    switch (typeof item) {
      case 'string':
        // ::after or ::before
        var value = name === 'content'
          ? JSON.stringify(item)
          : item;
        cssList.property.push(toLowerCase(name) +': '+ value)
        break;
      case 'number':
        cssList.property.push(toLowerCase(name) +': '+ item)
        break;
      case 'object':
        var pureKey = name.replace(/^\$/, '');
        if (pseudoKey.indexOf(pureKey)) {
          cssList.pseudo[pureKey] = parseStyle(selector, item)
        } else {
          cssList.children.push(parseStyle(name, item))
        }
        break;
      default:
        console.warn('Invalid css property or selector.')
        break
    }
  } return cssList
}

module.exports = parseStyle
