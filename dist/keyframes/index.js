function keyframes(options) {
  this.keyframes = options;
}

keyframes.prototype = {
  compile: function () {
    this.cssText = parseKeyFrames(this.keyframes);
    return this
  },
  append: function () {
    var styleEl = document.createElement('style');
    styleEl.type = 'text/css';
    styleEl.textContent = this.cssText.join('\n');
    document.getElementsByTagName('head')[0].appendChild(styleEl)
  }
}

keyframes.kernel = ['-webkit-', '-moz-', '-ms-', '-o-']

function parseKeyFrames (options) {
  var keyframes = {};
  for (var i in options) {
    keyframes[i] = testFrames(options[i])
  } return transformFrames(keyframes)
}

function testFrames (frames) {
  var result = [];
  for (var i in frames) {
    if (/^(\d{1,3}%$|from|to)/.test(i)) {
      result.push(i + ' { \n' + objectToArray(frames[i]).join('; \n') + '\n }')
    }
  } return result
}

function objectToArray (obj) {
  var array = [];
  for (var i in obj) {
    array.push(i + ': ' + obj[i])
  } return array
}

function transformFrames (frames) {
  var cssRule = [];
  var kernel = JStyleSheet.keyframes.kernel;
  for (var i in frames) {
    cssRule.push('@keyframes' + ' ' + i + ' { \n ' + frames[i].join('\n') + '\n }')
    for (var k = 0; k < kernel.length; k++) {
      cssRule.push('@' + kernel[k] + 'keyframes' + ' ' + i + ' { \n' + frames[i].join('\n') + ' \n }')
    }
  } return cssRule
}

module.exports = keyframes
