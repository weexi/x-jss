var isEmptyObject = require('./util/isEmptyObject')

/**
 * @description
 * @param sheet
 * @returns {Array}
 */
function compile (sheet) {
  var result = [];
  for (var item in sheet) {
    var current = sheet[item];
    if (item === 'property') {
      if (current.length) {
        result.push(sheet.selector + ' { \n' + current.join('; \n') + ' \n}')
      }
    } else if (item === 'children') {
      for (var i = 0; i < current.length; i++) {
        var $item = current[i];
        if (/^&\.[\w-\W]+/.test($item.selector)) {
          $item.selector = sheet.selector + '' + $item.selector.replace(/^&/, '')
        } else if (/^&(nth-child\([1-9]\)|first-child|last-child|not\([\w-\W]+\))/.test($item.selector)) {
          $item.selector = sheet.selector + ':' + $item.selector.replace(/^&/, '')
        } else if (/^&::[\w-\W]+/.test($item.selector)) {
          $item.selector = sheet.selector + '::' + $item.selector.replace(/^&::/, '')
        } else {
          $item.selector = sheet.selector + ' ' + $item.selector
        }
        result = result.concat(compile($item))
      }
    } else if (item === 'pseudo' && !isEmptyObject(current)) {
      for (var $$item in current) {
        current[$$item].selector = sheet.selector + ':' + $$item;
        result = result.concat(compile(current[$$item]))
      }
    }
  } return result
}

module.exports = compile
