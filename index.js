var parseStyle = require('./dist/parseStyle')
var compile = require('./dist/compile')
var isObject = require('./dist/util/isObject')
var keyframes = require('./dist/keyframes')

function JStyleSheet(options) {
  this.sheet = [];
  this.cssText = [];
  if (isObject(options)) {
    for (var item in options) {
      this.sheet.push(parseStyle(item, options[item]))
    }
  } else {
    console.error('Invalid StyleSheet in constructor')
  }
}

JStyleSheet.prototype = {
  toJSON: function () {
    return JSON.stringify(this.sheet)
  },
  compile: function () {
    for (var i = 0; i < this.sheet.length; i++) {
      this.cssText.push(compile(this.sheet[i]))
    }
    return this
  },
  append: function () {
    var result = [];
    var styleEl = document.createElement('style');
    for (var i = 0; i < this.cssText.length; i++) {
      result = result.concat(this.cssText[i])
    }
    styleEl.type = 'text/css';
    styleEl.textContent = result.join('\n');
    document.getElementsByTagName('head')[0].appendChild(styleEl)
  }
}

JStyleSheet.keyframes = keyframes

module.exports = JStyleSheet
